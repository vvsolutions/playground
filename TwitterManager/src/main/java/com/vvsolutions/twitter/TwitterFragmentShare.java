package com.vvsolutions.twitter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.support.annotation.NonNull;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.ActionBarActivity;
import android.view.ContextThemeWrapper;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import twitter4j.Status;

/**
 * Created by Bervimo on 1/11/14.
 */
public class TwitterFragmentShare extends DialogFragment {

    public static abstract class TwitterShareListener {

        public void onStartSharing() {}
        public void onCancelSharing() {}
        public void onEndSharing(Status result) {}
        public void onErrorSharing(Throwable error) {}
    }

    private String message;
    private Bitmap image;
    private TwitterShareListener twitterShareListener;

    public static TwitterFragmentShare newInstance(String message, Bitmap image, TwitterShareListener twitterShareListener) {

        TwitterFragmentShare twitterFragmentShare = new TwitterFragmentShare();
        twitterFragmentShare.message = message;
        twitterFragmentShare.image = image;
        twitterFragmentShare.twitterShareListener = twitterShareListener;

        return twitterFragmentShare;
    }

    public static TwitterFragmentShare newInstance(String message, TwitterShareListener twitterShareListener) {

        return TwitterFragmentShare.newInstance(message, null, twitterShareListener);
    }

    public static TwitterFragmentShare newInstance(TwitterShareListener twitterShareListener) {

        return TwitterFragmentShare.newInstance(null, null, twitterShareListener);
    }

    public static TwitterFragmentShare newInstance() {

        return TwitterFragmentShare.newInstance(null, null, null);
    }

    @Override
    public void onAttach(final Activity activity) {
        super.onAttach(activity);

        final boolean isConnected = TwitterManager.getInstance().isConnected(activity, new TwitterConnectListener() {

            @Override
            public void onConnect() {

                TwitterFragmentShare.newInstance(message, image, twitterShareListener)
                        .show(((ActionBarActivity) activity).getSupportFragmentManager(), getTag());
            }

            @Override
            public void onConnectError(Throwable error) {

                Toast.makeText(activity, error.getMessage(), Toast.LENGTH_SHORT).show();
            }
        });

        if (!isConnected) {

            dismiss();
        }
    }

    @NonNull
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {

        final ViewGroup container = (ViewGroup) LayoutInflater.from(getActivity()).inflate(R.layout.alert_share, null, false);
        final EditText editText = (EditText)container.findViewById(R.id.share_message);
        editText.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View view, boolean hasFocus) {

                    getDialog().getWindow().setSoftInputMode(
                            hasFocus ? WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_VISIBLE
                                     : WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
            }
        });

        if (message != null && message.length() > 0) {

            editText.setText(message);
            editText.setSelection(message.length());
        }

        final ImageView imageView = (ImageView)container.findViewById(R.id.share_image);

        if (image != null) {

            imageView.setImageBitmap(image);
        }
        else imageView.setVisibility(View.GONE);

        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.AlertDialogShare));
        builder.setView(container)
                .setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        message = editText.getText().toString();

                        new Handler(Looper.getMainLooper()).post(new Runnable() {

                            @Override
                            public void run() {

                                if (message != null && message.length() > 0) {

                                    shareAction();
                                }
                                else {

                                    Toast toast = Toast.makeText(getActivity(), R.string.twitter_not_share_message, Toast.LENGTH_SHORT);
                                    toast.setGravity(Gravity.CENTER, 0, 0);
                                    toast.show();

                                    TwitterFragmentShare.newInstance(twitterShareListener).show(getFragmentManager(), getTag());
                                }
                            }
                        });
                    }
                })
                .setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int id) {

                        new Handler(Looper.getMainLooper()).post(new Runnable() {

                            @Override
                            public void run() {

                                if (twitterShareListener != null) {

                                    twitterShareListener.onCancelSharing();
                                }
                            }
                        });

                    }
                }).create();

        return builder.create();
    }

    private void shareAction() {

        if (twitterShareListener != null) {

            twitterShareListener.onStartSharing();
        }

        TwitterManager.getInstance().updateStatus(getActivity(), message, image, new TwitterListener<Status>() {

            @Override
            public void onTwitterRequestSucceeded(Status result) {

                if (twitterShareListener != null) {

                    twitterShareListener.onEndSharing(result);
                }
            }

            @Override
            public void onTwitterRequestError(Throwable error) {

                if (twitterShareListener != null) {

                    twitterShareListener.onErrorSharing(error);
                }
            }
        });
    }
}
