package com.vvsolutions.twitter;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;

import java.util.List;

import twitter4j.JSONObject;
import twitter4j.Status;

/**
 * Created by Bervimo on 19/10/14.
 *
 */
public interface TwitterRequests {

    public boolean isConnected(Activity activity, TwitterConnectListener connectListener);
    public boolean onActivityResult(int requestCode, int resultCode, Intent data);

    public void updateStatus(Activity activity, String message, TwitterListener<Status> listener);
    public void updateStatus(Activity activity, String message, Bitmap bitmap, TwitterListener<Status> listener);

    public void showTimeline(Activity activity, String hashtag, TwitterListener<Status> listener);
    public void getTimeline(Activity activity, String hashtag, TwitterListener<List<Status>> listener);

    public void verifyCredentials(Activity activity, final TwitterListener<JSONObject> listener);

    void getFriendsFollowers(Activity activity, final TwitterListener<long[]> listener);

    public void clearCredentials(Context context);
}
