package com.vvsolutions.twitter;

import android.content.Context;
import android.media.Image;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.squareup.picasso.Picasso;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import twitter4j.Status;

/**
 * Created by Bervimo on 31/10/14.
 */
public class TwitterTimelineAdapter extends RecyclerView.Adapter<TwitterTimelineAdapter.ViewHolder> {

    private List<Status> statuses;
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("hh:mm");

    public TwitterTimelineAdapter() {

        this.statuses = new ArrayList<Status>();
    }

    public TwitterTimelineAdapter(List<Status> statuses) {

        this.statuses = statuses;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        private Context context;
        private TextView title;
        private TextView text;
        private TextView date;
        private ImageView image;

        public ViewHolder(View itemView, Context context) {
            super(itemView);

            this.context = context;

            title = (TextView)itemView.findViewById(R.id.cell_title);
            text = (TextView)itemView.findViewById(R.id.cell_text);
            date = (TextView)itemView.findViewById(R.id.cell_date);

            image = (ImageView)itemView.findViewById(R.id.cell_image);
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cell_timeline, viewGroup, false);

        return new ViewHolder(view, viewGroup.getContext());
    }

    @Override
    public void onBindViewHolder(ViewHolder viewHolder, int position) {

        Status status = statuses.get(position);

        viewHolder.title.setText(status.getUser().getName());
        viewHolder.text.setText(status.getText());
        viewHolder.date.setText(simpleDateFormat.format(status.getCreatedAt()));

        Picasso.with(viewHolder.context)
                .load(status.getUser().getProfileImageURLHttps())
                .resize(100, 100)
                .into(viewHolder.image);
    }

    @Override
    public int getItemCount() {

        return statuses.size();
    }
}
