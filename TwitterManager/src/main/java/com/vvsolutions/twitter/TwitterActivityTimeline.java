package com.vvsolutions.twitter;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import java.util.List;

import twitter4j.Status;

/**
 * Created by Bervimo on 31/10/14.
 */
public class TwitterActivityTimeline extends ActionBarActivity {

    protected String hashtag;
    protected RecyclerView recyclerView;
    protected SwipeRefreshLayout swipeRefreshLayout;
    protected ProgressBar progressBar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_twitter_timeline);

        if (getIntent().hasExtra("hashtag")) {

            hashtag = getIntent().getStringExtra("hashtag");
        }

        getSupportActionBar().setBackgroundDrawable(getResources().getDrawable(R.color.twitter));
        getSupportActionBar().setTitle("#" + hashtag);

        progressBar = (ProgressBar)findViewById(R.id.progress_bar);

        swipeRefreshLayout = (SwipeRefreshLayout)findViewById(R.id.swipe_layout);
        swipeRefreshLayout.setColorSchemeResources(R.color.twitter);
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {

            @Override
            public void onRefresh() {

                getTimeline();
            }
        });

        recyclerView = (RecyclerView)findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        recyclerView.addItemDecoration(new ItemDecorationDivider(this, ItemDecorationDivider.VERTICAL_LIST));
        recyclerView.setAdapter(new TwitterTimelineAdapter());

        getTimeline();
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        TwitterManager.getInstance().onActivityResult(requestCode, resultCode, data);
    }

    private void getTimeline() {

        swipeRefreshLayout.setRefreshing(true);

        TwitterManager.getInstance().getTimeline(this, hashtag, new TwitterListener<List<Status>>() {

            @Override
            public void onTwitterRequestSucceeded(List<Status> result) {

                progressBar.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);
                recyclerView.setAdapter(new TwitterTimelineAdapter(result));
            }

            @Override
            public void onTwitterRequestError(Throwable error) {

                progressBar.setVisibility(View.GONE);
                swipeRefreshLayout.setRefreshing(false);

                Toast.makeText(TwitterActivityTimeline.this, error.getMessage(), Toast.LENGTH_SHORT).show();

                finish();
            }
        });
    }
}
