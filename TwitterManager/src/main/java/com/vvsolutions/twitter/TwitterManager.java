package com.vvsolutions.twitter;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.support.annotation.Nullable;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.lang.ref.WeakReference;
import java.util.List;

import twitter4j.HttpClient;
import twitter4j.HttpClientFactory;
import twitter4j.HttpParameter;
import twitter4j.IDs;
import twitter4j.JSONObject;
import twitter4j.Query;
import twitter4j.QueryResult;
import twitter4j.Status;
import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.auth.RequestToken;
import twitter4j.conf.ConfigurationBuilder;

/**
 * Created by Bervimo on 16/10/14.
 *
 */
public class TwitterManager implements TwitterRequests {

    private static final String TWITTER_PREFERENCES = "twitter.preferences";
    private static final String ACCESS_TOKEN = "accessToken";
    private static final String ACCESS_TOKEN_SECRET = "accessTokenSecret";

    private static final int REQUEST_CODE_CALLBACK = 20887;

    private static boolean initialized = false;
    private static TwitterManager ourInstance = new TwitterManager();

    public static TwitterManager getInstance() {

        if (!initialized) {
            throw new RuntimeException("You must initialize TwitterManager with apiKey and apiSecret first.");
        }

        return ourInstance;
    }


    private SharedPreferences sharedPreferences;
    private SharedPreferences.Editor editor;

    private Twitter twitter;
    private ProgressDialog progressDialog;
    private TwitterConnectListener connectListener;
    private WeakReference<Activity> activity;

    private TwitterManager() {

        //
    }

    public static boolean isInitialized() {
        return initialized;
    }

    private static  String apiKey;
    private static String apiSecret;

    public static boolean initialize(String apiKey, String apiSecret) {

        if (initialized) {
            return true;
        }

        if (apiKey.isEmpty() || apiSecret.isEmpty()) {
            return false;
        }

        initialized = true;
        TwitterManager.apiKey = apiKey;
        TwitterManager.apiSecret = apiSecret;
        return true;
    }

    @Override
    public boolean onActivityResult(int requestCode, int resultCode, Intent data) {

        boolean handled = false;

        if (requestCode == REQUEST_CODE_CALLBACK) {

            handled = true;

            if (resultCode == Activity.RESULT_OK) {

                final String oauthVerifier = data.getStringExtra("oauth_verifier");

                AsyncTask<Object, Object, Object> asyncTask = new AsyncTask<Object, Object, Object>() {

                    @Override
                    protected Object doInBackground(Object[] objects) {

                        try {

                            AccessToken accessToken = twitter.getOAuthAccessToken(oauthVerifier);
                            saveAccessToken(accessToken);

                            return accessToken;

                        } catch (Exception e) { return e; }
                    }

                    @Override
                    protected void onPostExecute(Object object) {

                        if (connectListener != null) {

                            if (object instanceof AccessToken) {

                                connectListener.onConnect();
                            }
                            else connectListener.onConnectError((Throwable)object);
                        }
                    }
                };

                asyncTask.execute();
            }
            else {

                if (connectListener != null) {

                    connectListener.onConnectError(new Throwable("Canceled by the user..."));
                    connectListener = null;
                }
            }
        }

        return handled;
    }

    @Override
    public void updateStatus(final Activity activity, final String message, final TwitterListener<Status> listener) {

        updateStatus(activity, message, null, listener);
    }

    @Override
    public void updateStatus(final Activity activity, final String message, final Bitmap bitmap, final TwitterListener<Status> listener) {

        boolean isConnected = isConnected(activity, new TwitterConnectListener() {

            @Override
            public void onConnect() {

                updateStatus(activity, message, bitmap, listener);
            }

            @Override
            public void onConnectError(Throwable error) {

                if (listener != null) {

                    listener.onTwitterRequestError(error);
                }
            }
        });

        if (isConnected) {

            AsyncTask<Object, Object, Object> asyncTask = new AsyncTask<Object, Object, Object>() {

                @Override
                protected Object doInBackground(Object... objects) {

                    try {

                        StatusUpdate statusUpdate = new StatusUpdate(message);

                        if (bitmap != null) {

                            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, byteArrayOutputStream);

                            byte[] bitmapData = byteArrayOutputStream.toByteArray();
                            ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bitmapData);

                            statusUpdate.setMedia("image/jpeg", byteArrayInputStream);
                        }

                        return twitter.updateStatus(statusUpdate);

                    } catch (Exception exception) { return exception; }
                }

                @Override
                protected void onPostExecute(Object object) {

                    if (listener != null) {

                        if (object instanceof twitter4j.Status) {

                            listener.onTwitterRequestSucceeded((twitter4j.Status)object);
                        }
                        else listener.onTwitterRequestError((Throwable)object);
                    }
                }
            };

            asyncTask.execute();
        }
    }

    @Override
    public void showTimeline(final Activity activity, final String hashtag, final TwitterListener<Status> listener) {

        boolean isConnected = isConnected(activity, new TwitterConnectListener() {

            @Override
            public void onConnect() {

                showTimeline(activity, hashtag, listener);
            }

            @Override
            public void onConnectError(Throwable error) {

                if (listener != null) {

                    listener.onTwitterRequestError(error);
                }
            }
        });

        if (isConnected) {

            Intent intent = new Intent(activity, TwitterActivityTimeline.class);
            intent.putExtra("hashtag", hashtag);

            activity.startActivity(intent);
        }
    }

    @Override
    public void getTimeline(final Activity activity, final String hashtag, final TwitterListener<List<Status>> listener) {

        boolean isConnected = isConnected(activity, new TwitterConnectListener() {

            @Override
            public void onConnect() {

                getTimeline(activity, hashtag, listener);

            }

            @Override
            public void onConnectError(Throwable error) {

                if (listener != null) {

                    listener.onTwitterRequestError(error);
                }
            }
        });

        if (isConnected) {

            AsyncTask<Object, Object, Object> asyncTask = new AsyncTask<Object, Object, Object>() {

                @Override
                protected Object doInBackground(Object[] objects) {

                    try {

                        return twitter.search(new Query(hashtag));

                    } catch (Exception exception) { return exception; }
                }

                @Override
                protected void onPostExecute(Object object) {

                    if (listener != null) {

                        if (object instanceof QueryResult) {

                            QueryResult queryResult = (QueryResult)object;
                            listener.onTwitterRequestSucceeded(queryResult.getTweets());
                        }
                        else listener.onTwitterRequestError((Throwable)object);
                    }
                }
            };

            asyncTask.execute();
        }
    }

    @Override
    public void verifyCredentials(final Activity activity, final TwitterListener<JSONObject> listener) {

        boolean isConnected = isConnected(activity, new TwitterConnectListener() {

            @Override
            public void onConnect() {

                internalVerifyCredentials(activity, listener);
            }

            @Override
            public void onConnectError(Throwable error) {

                if (listener != null) {

                    listener.onTwitterRequestError(error);
                }
            }
        });

        if (isConnected) {

            internalVerifyCredentials(activity, listener);
        }
    }



    @Override
    public void getFriendsFollowers(final Activity activity, final TwitterListener<long[]> listener) {

        boolean isConnected = isConnected(activity, new TwitterConnectListener() {

            @Override
            public void onConnect() {

                internalGetFollowersFriends(listener);

            }

            @Override
            public void onConnectError(Throwable error) {

                if (listener != null) {

                    listener.onTwitterRequestError(error);
                }
            }
        });

        if (isConnected) {

            internalGetFollowersFriends(listener);

        }
    }

    private void internalGetFollowersFriends(final TwitterListener<long[]> listener) {
        new AsyncTask<Void, Void, Void>(){

            @Override
            protected Void doInBackground(Void... params) {

                try {

                    IDs ids = twitter.friendsFollowers().getFriendsIDs(-1);
                    listener.onTwitterRequestSucceeded(ids.getIDs());

                } catch (TwitterException e) {
                    e.printStackTrace();
                }

                return null;
            }
        }.execute();
    }


    private void internalVerifyCredentials(Activity activity, final TwitterListener<JSONObject> listener) {

        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage(activity.getResources().getString(R.string.twitter_loading));
        progressDialog.setCancelable(false);
        progressDialog.show();

        AsyncTask<Void, Object, Object> asyncTask = new AsyncTask<Void, Object, Object>() {

            @Override
            protected Object doInBackground(Void... params) {

                try {

                    HttpParameter[] parameters = new HttpParameter[] {
                            new HttpParameter("include_email", true),
                            new HttpParameter("include_entities", true),
                            new HttpParameter("skip_status", true)
                    };


                    HttpClient client = HttpClientFactory.getInstance(twitter.getConfiguration().getHttpClientConfiguration());
                    return client.get(twitter.getConfiguration().getRestBaseURL() + "account/verify_credentials.json", parameters, twitter.getAuthorization(), null).asJSONObject();


                } catch (TwitterException e) {
                    return e;
                }
            }

            @Override
            protected void onPostExecute(Object object) {

                if (listener != null) {

                    if (object instanceof JSONObject) {

                        listener.onTwitterRequestSucceeded((JSONObject)object);
                    }
                    else listener.onTwitterRequestError((Throwable)object);
                }

                progressDialog.dismiss();
            }
        };

        asyncTask.execute();
    }

    @Override
    public void clearCredentials(Context context) {

        if (sharedPreferences == null) {

            sharedPreferences = context.getSharedPreferences(TWITTER_PREFERENCES, Context.MODE_PRIVATE);
            editor = sharedPreferences.edit();
        }

        editor.clear();
        editor.commit();
    }


    private void updateTwitterConfiguration(@Nullable AccessToken accessToken) {

        ConfigurationBuilder configurationBuilder = new ConfigurationBuilder();
        configurationBuilder.setDebugEnabled(false)
                .setOAuthConsumerKey(apiKey)
                .setOAuthConsumerSecret(apiSecret);

        if (accessToken != null) {

            configurationBuilder
                    .setOAuthAccessToken(accessToken.getToken())
                    .setOAuthAccessTokenSecret(accessToken.getTokenSecret());
        }

        TwitterFactory factory = new TwitterFactory(configurationBuilder.build());
        twitter = factory.getInstance();
    }

    @Override
    public boolean isConnected(final Activity activity, final TwitterConnectListener connectListener) {

        this.activity = new WeakReference<>(activity);

        if (sharedPreferences == null) {

            sharedPreferences = activity.getSharedPreferences(TWITTER_PREFERENCES, Context.MODE_PRIVATE);
            editor = sharedPreferences.edit();
            editor.commit();
        }

        String token = sharedPreferences.getString(ACCESS_TOKEN, null);

        if (token != null) {

            String secret = sharedPreferences.getString(ACCESS_TOKEN_SECRET, null);

            AccessToken accessToken = new AccessToken(token, secret);
            updateTwitterConfiguration(accessToken);

            return true;
        }

        /*
            NOT LOGGED
        */

        this.connectListener = connectListener;

        updateTwitterConfiguration(null);

        progressDialog = new ProgressDialog(activity);
        progressDialog.setMessage(activity.getResources().getString(R.string.twitter_loading));
        progressDialog.setCancelable(false);
        progressDialog.show();

        AsyncTask<Object, Object, Object> asyncTask = new AsyncTask<Object, Object, Object>() {

            @Override
            protected Object doInBackground(Object... objects) {

                try {

                    return twitter.getOAuthRequestToken();

                }catch (Exception exception) { return exception; }
            }

            @Override
            protected void onPostExecute(Object object) {

                progressDialog.dismiss();

                if (object instanceof RequestToken) {

                    final Intent intent = new Intent(activity, TwitterConnectActivity.class);
                    intent.putExtra("authorizationURL", ((RequestToken)object).getAuthorizationURL());

                    activity.startActivityForResult(intent, REQUEST_CODE_CALLBACK);
                }
                else {

                    if (TwitterManager.this.connectListener != null) {

                        TwitterManager.this.connectListener.onConnectError((Throwable)object);
                    }
                }
            }
        };

        asyncTask.execute();

        return false;
    }

    private void saveAccessToken(AccessToken accessToken) {

        editor.putString(ACCESS_TOKEN, accessToken.getToken());
        editor.putString(ACCESS_TOKEN_SECRET, accessToken.getTokenSecret());
        editor.commit();

        updateTwitterConfiguration(accessToken);
    }
}
