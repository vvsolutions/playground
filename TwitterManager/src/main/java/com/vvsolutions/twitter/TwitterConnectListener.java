package com.vvsolutions.twitter;

/**
 * Created by Bervimo on 19/10/14.
 *
 */
public interface TwitterConnectListener {

    public void onConnect();
    public void onConnectError(Throwable error);
}
