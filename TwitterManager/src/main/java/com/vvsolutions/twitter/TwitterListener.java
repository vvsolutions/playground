package com.vvsolutions.twitter;

/**
 * Created by Bervimo on 19/10/14.
 */
public interface TwitterListener<T> {

    public void onTwitterRequestSucceeded(T result);
    public void onTwitterRequestError(Throwable error);
}
