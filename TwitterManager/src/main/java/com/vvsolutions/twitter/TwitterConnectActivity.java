package com.vvsolutions.twitter;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ProgressBar;

/**
 * Created by Bervimo on 16/10/14.
 *
 */
public class TwitterConnectActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_twitter_connect);

        if (getIntent().getExtras() != null) {

            final ProgressBar progressBar = (ProgressBar) findViewById(R.id.progressBar);
            progressBar.setVisibility(View.VISIBLE);

            WebView webView = (WebView) findViewById(R.id.webView);
            webView.setWebViewClient(new WebViewClient() {

                @Override
                public boolean shouldOverrideUrlLoading(WebView view, String url) {

                    Log.e("URL", url);

                    return overrideUrl(url);
                }

                @Override
                public void onPageFinished(WebView view, String url) {

                    progressBar.setVisibility(View.GONE);
                }
            });

            webView.loadUrl(getIntent().getStringExtra("authorizationURL"));
        }
    }

    private boolean overrideUrl(String url) {

        if (url.contains("oauth_token") && url.contains("oauth_verifier")) {

            try {

                Uri uri = Uri.parse(url);

                Intent data = new Intent();
                data.putExtra("oauth_token", uri.getQueryParameter("oauth_token"));
                data.putExtra("oauth_verifier", uri.getQueryParameter("oauth_verifier"));

                setResult(RESULT_OK, data);

                finish();

                return true;

            }catch (Exception e) {e.printStackTrace();}
        }
        else if (url.contains("denied")) {

            setResult(RESULT_CANCELED);

            finish();

            return true;
        }

        return false;
    }
}
