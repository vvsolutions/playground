package com.vvsolutions.playground;

import android.graphics.Rect;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;

import java.util.Hashtable;

/**
 * Created by Bervimo on 17/10/14.
 *
 */
public class ItemDecorationInset extends RecyclerView.ItemDecoration {

    private int numElements;
    private int numColumns;
    private int numRows;

    private int horizontalInset;
    private int verticalInset;

    private Hashtable<Integer, Integer> mappedData;

    public ItemDecorationInset(int horizontalInset, int verticalInset) {

        if (verticalInset <= 0) {

            throw new IllegalArgumentException("Insets must be > 0");
        }

        this.horizontalInset = horizontalInset;
        this.verticalInset = verticalInset;

        mappedData = new Hashtable<Integer, Integer>();
    }

    private void updateItemDecorator() {

        numRows = (int)Math.ceil(numElements /(float)numColumns);
        int currentIndex = 0;

        for (int i = 0; i < numRows; i++) {

            for (int j = 0; j < numColumns; j++) {

                mappedData.put(currentIndex, i);

                currentIndex++;
            }
        }
    }

    private void updateItemDecoratorIfNeeded(RecyclerView parent) {

        boolean needUpdate = false;

        int itemCount = parent.getAdapter().getItemCount();

        if (numElements != itemCount) {

            numElements = itemCount;

            needUpdate = true;
        }

        RecyclerView.LayoutManager layoutManager = parent.getLayoutManager();

        if (layoutManager instanceof GridLayoutManager) {

            int spanCount = ((GridLayoutManager)layoutManager).getSpanCount();

            if (numColumns != spanCount) {

                numColumns = spanCount;

                needUpdate = true;
            }
        }

        if (needUpdate) {

            updateItemDecorator();
        }
    }

    private boolean isLastRow(int position) {

        return mappedData.get(position) == numRows - 1;
    }

    private boolean isLastItem(int position) {

        return  position == numElements - 1;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, RecyclerView.State state) {

        updateItemDecoratorIfNeeded(parent);

        AdapterMain.MainViewHolder viewHolder = (AdapterMain.MainViewHolder)parent.getChildViewHolder(view);

        int position = viewHolder.getPosition();
        int row = mappedData.get(position);

        if (!isLastRow(position)) {

            int nextRow = mappedData.get(position + 1);

            if (row != nextRow) {

                outRect.right = horizontalInset;
            }
        }
        else {

            if (!isLastItem(position)) {

                outRect.right = horizontalInset;
            }

            outRect.bottom = verticalInset;
        }

        outRect.left = horizontalInset;
        outRect.top = verticalInset;
    }
}
