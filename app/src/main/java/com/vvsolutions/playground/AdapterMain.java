package com.vvsolutions.playground;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * Created by Bervimo on 17/10/14.
 *
 */
public class AdapterMain extends RecyclerView.Adapter<AdapterMain.MainViewHolder> {

    public class MainViewHolder extends RecyclerView.ViewHolder {

        private TextView textView;

        public MainViewHolder(View itemView) {
            super(itemView);

            textView = (TextView)itemView.findViewById(R.id.textView);
        }
    }

    @Override
    public AdapterMain.MainViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {

        View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.cell_main, viewGroup, false);

        return new AdapterMain.MainViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AdapterMain.MainViewHolder viewHolder, int position) {

        viewHolder.textView.setText("Position " + position);
    }

    @Override
    public int getItemCount() {
        return 100;
    }
}
