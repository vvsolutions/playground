package com.vvsolutions.playground;

import android.content.Intent;
import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.vvsolutions.twitter.TwitterListener;
import com.vvsolutions.twitter.TwitterManager;

import twitter4j.User;

public class ActivityMain extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_main);

        TwitterManager.initialize("XbLrf77He5NvQ3B7FmMRzGZG7", "JVV9YI0aIa3RzPV4wm1YuLLFo68ki0Be4kfM6RJL2B6rUKyVCM");

        RecyclerView recyclerView = (RecyclerView)findViewById(R.id.recycler_view);

        if (getResources().getConfiguration().orientation == Configuration.ORIENTATION_PORTRAIT) {

            recyclerView.setLayoutManager(new GridLayoutManager(this, 1));
            recyclerView.addItemDecoration(new ItemDecorationDivider(this, ItemDecorationDivider.VERTICAL_LIST));
        }
        else {

            recyclerView.setLayoutManager(new GridLayoutManager(this, 3));
            recyclerView.addItemDecoration(new ItemDecorationInset(10, 10));
        }

        recyclerView.setAdapter(new AdapterMain());
        recyclerView.addOnItemTouchListener(new RecyclerItemTouchListener(this, new RecyclerItemTouchListener.OnItemClickListener() {

            @Override
            public void onItemClick(View view, int position) {


                TwitterManager.getInstance().verifyCredentials(ActivityMain.this, new TwitterListener<User>() {

                    @Override
                    public void onTwitterRequestSucceeded(User result) {

                        TwitterManager.getInstance().getFriendsFollowers(ActivityMain.this, new TwitterListener<long[]>() {
                            @Override
                            public void onTwitterRequestSucceeded(long[] result) {
                                for(long id : result) {
                                    Log.e("friend", id + "");
                                }
                            }

                            @Override
                            public void onTwitterRequestError(Throwable error) {

                            }
                        });
                    }

                    @Override
                    public void onTwitterRequestError(Throwable error) {

                        Log.e("Error", error.getMessage() + "");
                    }
                });
            }
        }));
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (TwitterManager.getInstance().onActivityResult(requestCode, resultCode, data)) {

            return;
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.activity_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_clear_credentials) {

            TwitterManager.getInstance().clearCredentials(this);

            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
